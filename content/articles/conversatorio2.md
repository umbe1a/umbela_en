---
date: 2021-06-16
title: Conversatory 2
category: proyectos
title_image: media/Conv2.png
image: https://en.umbela.org/media/Conv2.png
carrusel: True
author: Fernanda Pérez Lombardini, J. Mario Siqueiros-García, Lakshmi Charli-Joseph, Patricia Pérez-Belmont, Hallie Eakin, Almendra Cremaschi, Tania Campos, Enrico Cresta, Mirna Inturias, Loni Hensler, David Manuel-Navarrete
spanish: https://umbela.org/blog/conversatorio-2/
---

### The sowing af shared dreams
### Blog + video  

“The sowing of shared dreams” emerges from the idea of polyculture, where we converse and live in plurality, diversity, and dialogue based on difference. In polyculture, not only the harvests are more diverse and the soils richer, but through differences, we acknowledge ourselves and enrich our sense of communality: I am with you and you with me, but no one ceases to be who one is.

In this second conversatory we talk about what can be, without limitations, without mental ties, looking towards the emptiness that we can occupy, what are also called utopias. Some like Enrico, Loni, Tania and Mirna have embarked on the task of transit from Utopias as dreams to Utopias as processes, from “what would be if…?”, to fill their hands with soil, tilling Utopias day by day, and making them bloom.

And of course, neither dreams nor utopias nor communality are an easy process; there may be stones in the way, soils may be badly eroded, and conditions may not be conducive to seed germination. "It's crazy - some say - without agrochemicals you won't make this land produce!" How many times have we not heard this? But those who venture into these crops have learned to live with the frosts, with the pests, with the lack of water and with too much sun.

Adding company, combining and articulating different ways of knowing and questioning our beliefs and values allows us to meet those for whom, we are the Others and vice versa. Transforming the soil to make something grow where others said nothing would grow, requires changes in oneself and these changes are not necessarily comfortable, in fact, if they are profound, true transformations almost by nature have to generate discomfort. But when we transform these profound changes into underground bridges and share them, we transcend the otherness and create a new identity.

In addition to the changes that are generated individually and collectively, the sowing of dreams also requires weaving different times and spaces. Leaving aside the linear and imperative time that has been imposed on us to encounter other figurative, social and physical spaces, can lead us towards the exploration of different ideas of time, such as the time needed to carry and sow the seeds in the field, the time lived, the time of the Earth...

This conversation is full of seeds of dreams to be explored and germinated dreams to be savoured. There is so much to say that ideally we would have a chat around a fire, following the rhythm of the fields, to attend to that which does not respond with the immediacy that today's culture has conditioned us to demand...


<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/572822589?h=b893641e64&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="La siembra de sue&amp;ntilde;os compartidos"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

