---
date: 2021-11-20
title: Conversatory 4
category: proyectos
title_image: media/Conv4.png
image: https://umbela.org/media/Conv4.png
carrusel: True
author: J. Mario Siqueiros-García, Lakshmi Charli-Joseph, Marila Lázaro, María Mancilla-García, Ingrid Estrada y Patricia Pérez-Belmont
spanish: https://umbela.org/blog/conversatorio-4/
---

### Caring for and nurturing the soil
### Blog + video  

The theme of the fourth conservatory 'Caring for and nurturing the soil' is the care of transformation processes-spaces from two complementary senses:
1.	that of dedicating the attention, dedication and time to designing and implementing processes to enable transformations, observing the details such as ensuring that each of the participating voices is heard, the transparency of the process, and the structure and quality of the dialogue;
2.	that of nurturing the processes just as those who cultivate the land nurture the soil, which implies looking deeply and doing what is necessary to ensure the well-being of a transhuman we.

The dialogue of this conversation runs between different points of the landscape that draw these two ways of thinking about the notion of care. In this space, beauty, the responsibility to care and the right to be cared for stand out. Beauty is rarely thought of in transformation processes; However, this conservatory talks about its importance as a powerful support element, which motivates and hooks us with these long-term processes.

And there are also death, ruins, mourning and wounds, as well as the longing for community, for connecting with what is different. Death and wounds are motivation for the collective embrace and the ruins, like beauty, are the support, the marks in the territory of its history, of what has been before, of what is there that we overlook, or that many times it is invisible to us but that without them the transformation processes in which we are involved would not be possible.

In this discussion we also return to the theme of utopias that are linked to beauty and the future that can be and that connect the temporal aspect by being a vision of the future that affects our doing of the present, while inviting us to think about the role of the ruins in the projection of the future ... a future where we take care of ourselves, -with a sense of ourselves beyond the human- to act accordingly and procure what can lead in that direction, transcending the agency of the subjects.

We share fragments of this conversation in which the role of facilitators as articulators of voices and knowledges is also highlighted, a role that must be taken from a deep listening and genuine humility to care for and nurture transformation processes.

<center>
<img alt="Conversatory 4" src="/media/conv4drawing.png" width="80%" />
</center>

<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/663792426?h=661f730e02&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Cuidando y nutriendo la tierra.m4v"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
