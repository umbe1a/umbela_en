---
date: 2021-11-20
title: Conversatory 3
category: proyectos
title_image: media/Conv3.png
image: https://umbela.org/media/Conv3.png
carrusel: True
author: Mariana Benítez, Patricia Pérez-Belmont, Helda Morales, Aranzazú Díaz, Flor Ciocchini, Luis Bracamontes, Lakshmi Charli-Joseph
english: https://en.umbela.org/blog/conversatory-2/
---

### Gathering knowledges
### Blog + video  

“Gathering knowledges” is a conversation where we speak from the collective, from the organisations that have sown diverse seeds and that, with each cycle of cultivation, have collected mistakes, successes and understanding that have allowed them to evolve. From the conversation we had, this 'exquisite corpse' emerged (wrote by Mariana Benítez and narrated by Hallie Eakin), which captures the main insights that speakers shared with us:

<center>
<iframe src="https://archive.org/embed/poem-conv-3" width="500" height="30" frameborder="0" webkitallowfullscreen="true" mozallowfullscreen="true" allowfullscreen></iframe>
</center>

<center>
<p>
<em>
Sowing, cultivating and harvesting are happening all the time; each time that we harvest, 
new understanding nourishes the organisation; harvesting knowledge is not linear, we do not 
educate ourselves alone, nor as one person to another, we educate ourselves together, 
harvesting and sowing, harvesting and sowing, and much will die, atmospheres of learning in 
which we see each other as equals ; the dialectic between the individual and the collective, 
is sometimes disruptive.

Its there that time enters: there, pausing, we can re-signify important movements that could 
have continuity in other places, and in other times; Sometimes I am the pest, we with our “run, run”, 
sometimes I've had to set fire and take the harvest from the ashes, slash and burn, I've had to do that 
to myself, you have to know when to withdraw and let things grow.

Life on the edge of the abyss, in constant contradiction, being outside and being inside, navigating 
in different conditions, including pandemics, sharing from organisation to organisation, work is never 
finished, it is always in movement, there are no recipes, only principles, bringing harvest through cooking, 
food excites us and connects us, a classroom-kitchen-laboratory, through eating we advance agroecology, 
humility, listening, knowing that we all have something to contribute, looking at ourselves as a “milpa”, 
with our different parts that can grow separately but better together, we set ourselves to cook, to eat, 
to work on a play or write a poem.

In the academy… What is it that’s of value? is it really going to achieve what we want?, the organisation 
starts with the broom, meeting, work, party, the encounters arising from cooking or art or party, a nakedness 
to face conflicts, celebration, mischief and something to share, to see each other with different eyes, 
to be prepared to challenge and to be challenged, to irritate, to make ourselves uncomfortable and  move.
</p>
</em>
</center>

<div style="padding:56.27% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/651264991?h=6fa86ec133&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Cosechando saberes"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>
