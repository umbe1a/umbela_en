---
date: 2021-04-16
title: Conversatory 1
category: proyectos
title_image: media/Conv1.png
image: https://en.umbela.org/media/Conv1.png
carrusel: True
author: David Manuel-Navarrete, Almendra Cremaschi, Patricia Pérez-Belmont, Lakshmi Charli-Joseph, J. Mario Siqueiros-García, Hallie Eakin 
spanish: https://umbela.org/blog/conversatorio-1/
---

### Regenerating the soil for new roots
### Blog + video  

What you will see in this first video is a conversation over 'low flame', in which four knowledges 
cultivators from Colombia and Mexico 'dig' into the roots of their own knowledge systems, and share 
how through experience and reflection, they have transitioned towards more sustainable and plural ways 
of seeing and understanding the world. These cultivators question the forms of knowledge that were 
imposed on them and how they have opened themselves, not without conflict, to new voices, ideas, 
elements and substances that have nourished and transformed their ways of apprehending and cultivating. 

Their proposals and reflections include: 

- Question "enclosed" or "boxed-in" thinking and methods which, while producing certain results, leave people seeking transformations towards sustainability with a feeling of emptiness.
- Recognise that we may face uncomfortable and frustrating situations, with barriers that test our tolerance or may even break our innocence but in the end, give us the freedom to embrace new attitudes that accept difference and diversity.
- Revalue methods of conversing and building bridges with the non-human (e.g., with the climate, the organisms, the mountain).
- Celebrate differences from a foundation of empathy based on the elements that are common to us all.

Some of the questions that emerge from the conversation are: Is it possible to transform the ways in which we perceive and understand what surrounds us? What structures do we need to break down? Who can we count on in this journey? What are the opportunities to transform ourselves? What challenges does this transformation imply?

Listen in this video these and other reflections on how to take advantage of the cracks in our own soil, and how to aerate it, decompact it and, ultimately, regenerate it in order to collectively build  a global soil suitable for a polyculture of knowledges and ways of knowing.


<div style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/541131729?badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Regenerando el suelo para nuevas raíces"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>


<p>
<em>  "Within and across the various strands in this particular conversation, ‘social’ and ‘natural’ implications unfold inseparably together. A resulting vision emerges of deeply entangled political ecologies: in which ambiguous relations and flowing processes matter more than rigid categories and fixed structures. That all this is achieved through the little rectangular frames of Zoom, is all the more to the authors’ credit. I can’t wait for the next instalment…"           Andy Stirling
</em>
</p>
