---
title: Advocacy
subtitle: Advocacy
slug: projects/advocacy
no_banner: True
status: hidden
english: https://umbela.org/proyectos/incidencia/
---


<center>
<object type="image/svg+xml" data="/theme/images/projects.svg" width="40%" height="100%"></object>
</center>

## Advocacy

Advocacy activities in which we have contributed with some of Umbela's approaches to 
promote transformative narratives in both outreach and decision-making spaces. 

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-4">
<h2>Transformative Water Pact</h2>
UN Water Conference 2023
<center>
<a href= "https://transformativewaterpact.org/">
<img alt="TWP" src="/media/twp.jpg" width="80%" />
</a>
</center>
</div>

<div class="col-md-4">
<h2>Así se ve el agua en México</h2>
Collective photographic documentation on the water situation in Mexico.
<center>
<a href= "https://susmai.sdi.unam.mx/index.php/en/actividades/documentacion-fotografica">
<img alt="AAM" src="/media/asiseveelagua.jpeg" width="80%" />
</a>
</center>
</div>

<div class="col-md-4">
</div>

</div>

----
