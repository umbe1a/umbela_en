---
title: Articulation of voices
subtitle: Articulation of voices
slug: projects/articulationvoices
no_banner: True
status: hidden
spanish: https://umbela.org/proyectos/articulacionvoces/ 
---

<center>
<object type="image/svg+xml" data="/theme/images/projects.svg" width="40%" height="100%"></object>
</center>

## Articulation of voices

We invite you to learn about some of the projects in which we have collaborated 
with the design and facilitation of participatory processes to create spaces for dialogue 
and articulation of voices to promote trajectories of transformation towards sustainability. 

<h6><i>Sorry, some links are not yet enabled...</i></h6>

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-4">
<h3>Climate Action System for the Seas and Coasts of Yucatan</h3>
Participatory workshops to establish strategies for climate change in the 
Yucatan Peninsula, 2023. 
<center>
<a href= "https://umbela.org/cultivando-saberes/">
<img alt="sacmyc2023" src="/media/proyectos/sacmyc2023.jpeg" width="60%" />
</a>
</center>
</div>

<div class="col-md-4">
<h2>Participatory mapping of possible futures</h2>
Elaboration of a futures model of 
San Juan Tlacotenco, Morelos 2023
<center>
<img alt="cartografiapart" src="/media/cartografiapart.jpg" width="80%" />
</center>
</div>

<div class="col-md-4">
<h3>"One Health: from the prevention of emerging zoonotic diseases"</h3>
Dialogues for prevention strategies in the 
Yucatan Peninsula 2022.
<center>
<img alt="onehealth" src="/media/proyectos/onehealth.jpeg" width="80%" />
</center>
</div>

</div>

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-4">
<h3>Experiences exchange among producers</h3>
Milpa Alta, Xochimilco y Tláhuac, CdMx 2022
<center>
<img alt="proyecto2" src="/media/encuentro.jpg" width="80%" />
</center>
</div>

<div class="col-md-4">
<h3>The coastal city we dream of</h3>
Participatory workshops with youth <br/>
Los Cabos, Boca del Río, Bacalar, Chetumal - 2022
<center>
<a href= "https://www.youtube.com/watch?v=BgRbDd17YIQ">
<img alt="taller1" src="/media/bacalar.JPG" width="80%" />
</a>
</center>
</div>

<div class="col-md-4">
<h3>Transdisciplinary Network for Sustainable Livestock Production</h3>
Yucatan Peninsula 2021 - 2022
<center>
<img alt="taller2" src="/media/yucatan.jpeg" width="80%" />
</center>
</div>

</div>

----
