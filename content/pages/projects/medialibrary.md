---
title: Media library
subtitle: Media library
slug: projects/medialibrary
no_banner: True
status: hidden
spanish: https://umbela.org/proyectos/mediateca/
---

<center>
<object type="image/svg+xml" data="/theme/images/projects.svg" width="40%" height="100%"></object>
</center>

## Media library

Consult a diversity of digital content 
(podcasts, poems, quick reads, booklets and more).

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-4">
<h3>and this is why i need you</h3>
Poem of Susi Moser
<center>
<a href= "https://umbela.org/blog/and-this-is-why-i-need-you/">
<img alt="connections" src="/media/connections.png" width="80%" />
</a>
</center>
</div>

<div class="col-md-4">
<h3>El viaje de Ronin</h3>
Booklet - Transgressive learning
<center>
<a href= "https://umbela.org/blog/el-aprendizaje-transgresivo-como-un-catalizador-de-cambio-profundo">
<img alt="AT" src="/media/portada_tinyb.png" width="60%" />
</a>
</center>
</div>

<div class="col-md-4">
<h2>Regenerating the soil for new roots</h2>
Video + blog - Project "Cultivating knowledges"
<center>
<a href= "https://en.umbela.org/blog/conversatory-1/">
<img alt="Conv1" src="/media/Conv1.png" width="80%" />
</a>
</center>
</div>

</div>


<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-4">
<h2>The sowing af shared dreams</h2>
Video + blog - Project "Cultivating knowledges"
<center>
<a href= "https://en.umbela.org/blog/conversatory-2/">
<img alt="Conv2" src="/media/Conv2.png" width="80%" />
</a>
</center>
</div>

<div class="col-md-4">
<h2>Gathering   knowledges</h2>
<p>Video + blog - Project "Cultivating knowledges"</p>
<center>
<a href= "https://en.umbela.org/blog/conversatory-3/">
<img alt="Conv3" src="/media/Conv3.png" width="80%" />
</a>
</center>
</div>

<div class="col-md-4">
<h2>Caring for and nurturing the soil</h2>
Video + blog - Project "Cultivating knowledges"
<center>
<a href= "https://en.umbela.org/blog/conversatory-4/">
<img alt="Conv4" src="/media/Conv4.png" width="80%" />
</a>
</center>
</div>

</div>

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-4">
<h3>Waves of challenging research for sustainability</h3>
Blog - Methods series
<center>
<a href= "https://steps-centre.org/blog/waves-of-challenging-research-for-sustainability/">
<img alt="waves" src="/media/waves-of.jpg" width="80%" />
</a>
</center>
</div>

<div class="col-md-4">
<h3>Transdisciplinary methods, relationships, politics and praxis</h3>
Blog - Methods series
<center>
<a href= "https://steps-centre.org/event/challenging-research-for-sustainability-transdisciplinary-methods-relationships-politics-and-praxis/">
<img alt="CR" src="/media/challenging-research.jpg" width="80%" />
</a>
</center>
</div>


<div class="col-md-4">
<h3>The ‘methods bazar’ </h3>
Blog - What did we learn about transdisciplinary methods for sustainability?
<center>
<a href= "https://steps-centre.org/blog/the-methods-bazaar-what-did-we-learn-about-transdisciplinary-methods-for-sustainability/">
<img alt="methodsbazaar" src="/media/methodsbazaar.png" width="80%" />
</a>
</center>
</div>

</div>

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-4">
<h3>Umbela launching</h3>
<a href="https://youtu.be/Opk4FNe-_xI">Virtual event</a> - We talked about our motivations, inspirations and our experience in putting our approaches into practice.
</i>
<center>
<a href= "https://umbela.org/blog/lanzamiento-del-proyecto-umbela">
<img alt="lanzamiento" src="/media/lanzamiento.png" width="80%" />
</a>
</center>
</div>

<div class="col-md-4">
</div>

<div class="col-md-4">
</div>

</div>
----
