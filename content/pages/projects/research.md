---
title: Research
subtitle: Research
slug: projects/research
no_banner: True
status: hidden
spanish: https://umbela.org/proyectos/investigaci%C3%B3n/
---

<center>
<object type="image/svg+xml" data="/theme/images/projects.svg" width="40%" height="100%"></object>
</center>

## Research

Participation in academic projects.

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-4">
<h3>Water Transformation Pathways Planning</h3>
Xochimilco wetland 2023 - 2027
<center>
<a href="https://trans-path-plan.com/">
<img alt="wtpp" src="/media/proyectos/wtpp.png" width="60%" />
</a>
</center>
</div>

<div class="col-md-4">
<h3>Water mirrors</h3>
Xochimilco wetland 2023 - 2027
<center>
<a href="https://sites.google.com/view/water-mirrors-project/home">
<img alt="watermirrors" src="/media/proyectos/watermirrors.png" width="100%" />
</a>
</center>
</div>

<div class="col-md-4">
<h3>Cultivating  knowledges</h3>
Series of videos, blogs, audio poems and images to explore how to 
cultivate knowledges to foster transformations.  
<center>
<a href= "https://en.umbela.org/cultivating-knowledges/">
<img alt="CS" src="/media/conversatorios_fotos.png" width="80%" />
</a>
</center>
</div>

</div>

----
