---
title: REDES A.C.
role: MSc. Elsa Valiente Riveros, General Director
slug: equipo/redes
headshot: /media/logo_redes.png
status: hidden
template: perfil
spanish: https://umbela.org/equipo/redes/
---

<br />

Director of [Redes](https://www.redesmx.org/)


_Promotion and implementation of ecological conservation and restoration programs and projects linked to social development; Linking scientific and traditional knowledges_


<https://www.redesmx.org/>

<br />


