---
title: ASU 
role: Prof. Hallie Eakin,     Dr. David Manuel-Navarrete
slug: equipo/asu
headshot: /media/imagen_temporal_asu.png
status: hidden
template: perfil
spanish: https://umbela.org/equipo/asu/
---

<br />

Prof. Hallie Eakin

Research fellow – School of Sustainability, College of Global Futures, Arizona State University

_Climate change adaptation, vulnerability and resilience, transformations towards sustainability_

<https://sustainability-innovation.asu.edu/person/hallie-eakin/>

-----

Dr. David Manuel-Navarrete

Associate Professor – School of Sustainability, College of Global Futures, Arizona State University

_Climate change governance, ecological integrity, socio-ecological inequity, knowledge systems for sustainable agriculture_

<https://sustainability.asu.edu/person/david-manuel-navarrete/>
