---
title: STEPS Centre
role: Prof. Andy Stirling, Dr. Marina Apgar
slug: allies/steps_centre
headshot: /media/logo_steps.png
status: hidden
template: perfil
spanish: https://umbela.org/equipo/steps_centre/
---

<br />

Prof. Andrew Stirling

Researcher - Science Policy Research Unit (SPRU) & Co-director STEPS Centre, Sussex University.

_Transdisciplinary 'publicly-engaged' research, policy advise on the politics of science, technology and innovation; Uncertainty, participation, diversity and sustainability in the governance of science and innovation_

<https://profiles.sussex.ac.uk/p7513-andrew-stirling>

----

Dr. Marina Apgar

Research Fellow - Participation, Inclusion and Social Change Cluster, Institute of Development Studies (IDS) & STEPS Centre, Sussex University.

_Participatory Action Research; Methodologies for planning, monitoring and evaluation of participatory and transdisciplinary processes_ 

<https://www.ids.ac.uk/people/marina-apgar/>
