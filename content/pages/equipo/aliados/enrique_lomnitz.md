---
title: Isla Urbana
role: Enrique Lomnitz, General Director
slug: allies/isla_urbana
headshot: /media/logo-isla-urbana.png
status: hidden
template: perfil
spanish: https://umbela.org/equipo/isla_urbana/
---

<br />


Director of [Isla Urbana](https://islaurbana.org/)


_Building decentralized infrastructure for sustainable water access, especially for Mexico's most marginalized areas; Regenerative and local development; Rainwater harvesting systems_


<https://islaurbana.org/>

<br />


