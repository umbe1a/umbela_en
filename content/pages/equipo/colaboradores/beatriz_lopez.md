---
title: Dr. Beatriz López Arboleda
role: Collaborator
slug: equipo/beatriz-lopez
headshot: /media/Beatriz_Lopez.jpeg
status: hidden
template: perfil
spanish: https://umbela.org/equipo/beatriz-lopez/
---

Beatriz has the ability to work with interdisciplinary groups in different ecosystem scenarios such as forest conservation areas, oceanic ecosystems (Colombian Pacific), wetlands, Andean forests and border areas between countries such as the Amazonian Triple Border Brazil-Colombia and Peru. She has participated in the development of projects at the community level and at different geographical scales, with a diversity of organisational and institutional actors under sustainable schemes. In this context, she has also formulated and executed, in a concerted manner, lines of action around agroecological transition in indigenous, peasant and afro-descendant communities. Beatriz has also had the great opportunity to apply her knowledge to the characterisation of socio-environmental conflicts caused by situations of armed conflict in Colombia and by the existence of mega-projects. Beatriz is enthusiastic about the planning and execution of community and academic events, as well as the application of participatory tools and working together with artistic contributions such as graphic design, photographs and short films that contribute to the circulation of knowledge and information.