---
title: Dr.(c) Almendra Cremaschi
role: Collaborator
slug: partner/almendra-cremaschi
headshot: /media/almendra-cremaschi.jpg
status: hidden
template: perfil
spanish: https://umbela.org/equipo/almendra-cremaschi/
---

_Sustainable agronomy, participatory processes, collective intelligence, collaborative innovation_

Agronomist from the National University of La Plata, Argentina and PhD candidate from the same University (FCAyF-UNLP) with the research entitled "Emergence of niches from base innovations in the seed system of Argentina", with the support of a doctoral scholarship granted by the National Council for Scientific and Technical Research (CONICET).
Currently, she is working at the [Research Center for Transformation](http://fund-cenit.org.ar/) (CENIT), of the National University of San Martín, where she is a lecturer of Sustainable Development.
Since 2018 she is a co-founder of [Bioleft](https://www.bioleft.org/en/), a community laboratory that seeks solutions to seed sustainability problems, based on the use of collective intelligence and open knowledge. Within the framework of this initiative, she has collaborated closely with the National Autonomous University of Mexico.

For more than ten years she has been working on topics related to sustainability, family farming, and participatory methodologies for the co-production of knowledge and innovations, both from rural extension and research on related themes. In particular, she is interested in knowledge co-production processes within the framework of transformative initiatives and in reflecting on and designing methodological strategies that contribute to building fair and open spaces for creativity and collaborative innovation.
