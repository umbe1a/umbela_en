---
title: Dr. Carlos Muñoz Piña
role: Collaborator
slug: partner/carlos_munoz
headshot: /media/Carlos_Munoz.jpg
status: hidden
template: perfil
spanish: https://umbela.org/equipo/carlos_munoz/
---

_Economic-environmental research, in particular the design of economic instruments_

Carlos Muñoz Piña has nearly 20 years of experience working on environmental, resource, 
and energy issues, mostly centered on economic research and policy design.   
He studied economics at the Instituto Tecnológico Autónomo de México (ITAM) in Mexico City, 
and has graduate studies in  Environmental and Resource Economics at the University College 
London and the University of California at Berkeley  His career has crossed both government 
and private policy research institutions. Among them, Mexico’s Instituto Nacional de Ecología 
y Cambio Climático (INECC) in the Ministry of the Environment -where his team designed the 
Payment for Environmental Services program, and the Centro Mario Molina for Sustainability 
-working on the design of Mexico’s carbon tax and Clean Energy Certificate, plus the economics 
departments of the Universidad Iberoamericana and ITAM.  Over the years Dr. Muñoz has served 
as advisor and board member for development banks, NGOs and international organizations, 
especially in energy and sustainability issues.  From 2014 to 2018, Dr. Muñoz Piña was 
Director General for Revenue Policy at the Undersecretariat for Revenues in Mexico’s 
Ministry of Finance, working, among other things, on taxation and pricing issues related 
to Mexico’s Energy Reform.  In 2019 he was appointed as Director for Research and 
Data Integrity at the World Resources Institute.  

<https://www.researchgate.net/profile/Carlos-Munoz-Pina>
