---
title: Msc. Elsa Valiente
role: Collaborator
slug: equipo/elsa-valiente
headshot: /media/Elsa_Valiente.jpeg
status: hidden
template: perfil
spanish: 
---

_Restoration of natural systems, participatory work, citizen scienceRestoration of natural systems, participatory work, citizen science._

With studies in Biological Sciences and an orientation in ecological restoration, Elsa has worked for the last 20 years in participatory work and citizen science, with farmers, students from elementary to professional level, and with the general public, with the aim of teaching the elements and relationships of natural systems and the impact that our daily productive and tourist actions have on them.

Elsa has managed and coordinated support and collaborations for the study of the ‘chinampas’ farming system and water quality in Xochimilco, which resulted in the training of farmers in the sub-basin of Xochimilco, Milpa Alta and Tláhuac in soil conservation techniques, agroecology and agrobiodiversity, and in the provision of green infrastructure to help improve the quality of water and agricultural products. 

The restoration of natural systems that provide resources for food and medicinal use also entails training users in sustainable productive activity, which was also carried out with the support of volunteers from relevant companies in the corporate sphere. Elsa believes that for the conservation of our natural systems and their biodiversity, it is fundamental to provide knowledge through different methods, whether conventional, participatory or artistic expressions.