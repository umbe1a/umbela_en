---
title: MSc Rodrigo García Herrera
role: Collaborator
slug: equipo/rodrigo-garcia
headshot: /media/Rodrigo_Garcia.jpg
status: hidden
template: perfil
spanish: https://umbela.org/equipo/rodrigo-garcia/
---

_Self-organization, complex networks, computational infrastructure_

Rodrigo is a Cybernetics Engineer and Master in Complexity Sciences. He is interested in deep socio-cultural change, through the development of self-managed horizontal organization platforms. 

He developed this website using [Gitlab](https://docs.gitlab.com/ee/user/project/pages/) and [Pelican](https://docs.getpelican.com/en/stable/quickstart.html).

### Profiles on the Internet
- [ResearchGate](https://www.researchgate.net/profile/Rodrigo_Garcia-Herrera)
- [ORCiD](http://orcid.org/0000-0002-7972-5746)
- [Google Scholar](https://scholar.google.com.mx/citations?user=aLFvcZQAAAAJ)

### Software Repositories
- [GitLab](https://gitlab.com/rgarcia-herrera)
- [GitHub](https://github.com/rgarcia-herrera)
