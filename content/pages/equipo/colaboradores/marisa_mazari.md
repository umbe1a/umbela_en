---
title: Dr. Marisa Mazari Hiriart
role: Collaborator
slug: equipo/marisa-mazari
headshot: /media/Marisa_Mazari.jpeg
status: hidden
template: perfil
spanish: https://umbela.org/equipo/marisa-mazari/
---

_Sustainable uses and monitoring of water_

Marisa is a Biologist from UNAM, with a Master's degree in Applied Hydrobiology from the University of Wales, Great Britain and a PhD in Environmental Science and Engineering from UCLA, USA. She is a Senior Researcher at the National Laboratory of Sustainability Sciences of the Institute of Ecology and Coordinator of the University Seminar on Society, Environment and Institutions (SUSMAI) at UNAM. She has worked in interdisciplinary groups on sustainable uses and monitoring of water in both urban and rural socio-ecosystems in Mexico. She has more than 100 publications and has formed professional capacities at undergraduate and postgraduate level. She is a National Researcher Level III of the SNI.

- [LANCIS-IE-UNAM](https://lancis.ecologia.unam.mx/personal/marisa_mazari/)
- [SUSMAI-UNAM](https://susmai.sdi.unam.mx/index.php/en/)