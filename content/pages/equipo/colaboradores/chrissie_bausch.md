---
title: Dr. Chrissie Bausch
role: Collaborator
slug: partner/chrissie_bausch
headshot: /media/chrissie_bausch.jpg
status: hidden
template: perfil
spanish: https://umbela.org/equipo/chrissie_bausch/
---


Chrissie is an interdisciplinary social scientist interested in how governance 
and policy affect sustainability and social equity. She has conducted research on 
natural resource management, land-use change, food systems, housing for special 
populations, stakeholder engagement, and climate change adaptation.


C. Bausch has a PhD in Sustainability from Arizona State University (ASU). 
She is a research analyst at ASU's Morrison Institute for Public Policy.


<https://www.linkedin.com/in/juliacbausch/>
