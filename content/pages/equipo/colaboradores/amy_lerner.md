---
title: Dr. Amy M. Lerner
role: Collaborator
slug: partner/amy-lerner
headshot: /media/amy-lerner.jpg
status: hidden
template: perfil
spanish: https://umbela.org/equipo/amy-lerner/
---

_Processes of landscape change, especially on the rural-urban frontier, sustainable aspects of city planning, including resilience, green infrastructure, and urban and peri-urban agriculture, and the science-policy interface_

Amy received her Ph.D. in Geography in 2011 from the University of California, 
Santa Barbara.  Subsequently she was a postdoctoral research associate at 
Rutgers University, NJ, in the departments of Geography and Human Ecology 
(2011-2013) and at the Princeton University School of Public and International 
Affairs (2013-2015).  

From 2015 to 2020, Dr. Lerner was an assistant professor in the 
National Laboratory for Sustainability Science in the Institute of Ecology,
National Autonomous University of Mexico (UNAM), where she led several 
research projects related to city-university partnerships, the persistence 
of peri-urban agriculture, and capacity-building in the local city government 
for risk management and resilience. She has extensive teaching experience in 
courses on sustainability science, transdisciplinary research methods, and 
global environmental change. Currently, she is an Associate Teaching 
Professor at UC San Diego. 

<https://usp.ucsd.edu/people/faculty/profiles/lerner_amy.html>
