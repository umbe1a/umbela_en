---
title: Dr. Edwin Hes
role: Collaborator
slug: partner/edwin_hes
headshot: /media/edwin-hes.jpg
status: hidden
template: perfil
spanish: https://umbela.org/equipo/edwin-hes/
---

_Research, capacity building and process facilitation in sustainable use of wetlands and nutrients fluxes modelling in wetland ecosystems_

Edwin Hes obtained an MSc from Wageningen University in 1998,
specializing in environmental science and technology. From 1999 to
2002 he worked for the Dutch Ministry of Economic Affairs as a public
consultant on European research grants.

His main research interests during the past years have been
sustainable use of wetlands and modelling nutrient flows in wetland
ecosystems, especially in papyrus wetlands in East Africa. He is
interested in the use of models to study ecosystem functions and
services and to evaluate socio-ecological systems. 

At IHE Delft, he has been coordinating the Environmental Science
programme from 2012 to 2016 and the joint degree programme in
Limnology and Wetland Management from 2012 until present. Hes has been
involved with teaching and training activities in the fields of
Environmental Systems Analysis, Environmental Modelling and Wetland
Management. 

In his educational activities he works with partner institutes from
Kenya (Egrton University), Austria (BOKU), Mexico (LANCIS-IE-UNAM) and
the Netherlands (WUR-CDI). Since 2007, Edwin has worked in research and capacity building projects
in more than 20 countries in Asia, Africa and Latin America with a
focus on sustainable use of wetlands. He is an experienced facilitator and moderator in an international
(development) setting.

<https://www.un-ihe.org/edwin-hes>
