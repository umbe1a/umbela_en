---
title: Dr. Emilio Rodríguez-Izquierdo
role: Collaborator
slug: partner/emilio_rodriguez
headshot: /media/emilio_rodriguez.png
status: hidden
template: perfil
spanish: https://umbela.org/equipo/emilio_rodriguez/
---

_Sustainability, socio-ecological modeling, resilience, environmental public policy (environmental impact assessment and ecological ordinance), and participatory processes in the management of natural resources_

Emilio has a PhD in Sustainability Science from the UNAM and is a National Researcher Candidate of the SNI.

In his PhD, he developed a methodological approach to determine the carrying capacity of gray whale watching vessels at Laguna Ojo de Liebre, in BCS. In 2020, he was a postdoctoral researcher at the IIS-UNAM in the project ¨Crisis Ambiental en México y Desigualdad¨. In this project, he carried out analysis of health data, access to water and sanitation, and its relationship with inequality, in three municipalities of the state of Chiapas, as well as some of the implications for the progress in the SDGs implementation in Mexico. 

He has worked at LANCIS-IE-UNAM as technical coordinator of the development and validation of indicators of Mexico City’s Resilience Strategy and as an environmental technician where he participated in the development of technical inputs for environmental planning instruments, such as the SEMARNAT's North Pacific Marine and Regional Ecological Ordinance Program. Emilio has experience as a specialist consultant in environmental impact assessment. In 2012 he had a brief voluntary collaboration with the DGPAIRS at SEMARNAT, where he participated in the integration of information for the formulation of the National Wetlands Policy and in the analysis of legislation related to the coastal zone in Latin American countries. As part of his master's degree in Environmental Studies from the Victoria University of Wellington in New Zealand, he developed a thesis on participatory processes in the management of the Cordillera Azul National Park, in Peru. 

Currently, Emilio is a consultant at Ithaca Environmental where he is involved in various projects with novel approaches (from issues on blue carbon in Mexico to the incorporation of vulnerability to climate change in risk atlases). 


<https://www.linkedin.com/in/emilio-rodríguez-izquierdo-mx>

