---
title: Dr. J. Mario Siqueiros-García
role: Collaborator
slug: partner/mario_siqueiros
headshot: /media/Mario_Siqueiros.jpg
status: hidden
template: perfil
spanish: https://umbela.org/equipo/mario_siqueiros/
---

_Computational social sciences, complex systems, sustainability sciences_

Full-time researcher at the Institute of Applied Mathematics and Systems Research 
(IIMAS) at UNAM, in Yucatán, México. Mario has a background in Anthropology and a 
PhD in Philosophy of Biology. He collaborates with researchers from the School of 
Sustainability at Arizona State University, Institute of Developmental Studies and 
STEPS Centre at Sussex University and the BeAnotherLab independent research project. 
Mario is a member and founder of the Transdisciplinarity, Art and Cognition (TACo) 
Collective. For the last three years, he has been co-organising the ALife & Society 
Special Session as part of the ALife conference.

His most recent work focuses on issues of sustainability and socio-ecological systems. 
In particular, he is interested in the role of affectivity in the sense of agency linked 
to socio-ecological problems. To this end, he works on the articulation of different 
quantitative tools such as complex network analysis, and qualitative tools such as 
in-depth interviews.

