---
title: Dr. Lakshmi Charli-Joseph
role: Scientific Adviser
slug: team/lakshmi_charli
headshot: /theme/images/equipo/lakshmi_charli.png
status: hidden
template: perfil
spanish: https://umbela.org/equipo/lakshmi_charli/
---

UNAM biologist, with a Master's degree in Environmental Law, Management and Policy (UAX) and a second on Environmental Planning and Management (IHE-Delft the Netherlands), both MSc thesis focused on Mexican river basins' policy and management. She holds a PhD in Sustainability Sciences (UNAM) with a research entitled "Promoting transformation pathways to sustainability through collective agency: the Transformation Laboratory in the social-ecological system of Xochimilco". She has a solid academic background in the planning and management of social-ecological systems and has professional experience in the design and facilitation of participatory and collaborative processes with transdisciplinary, multicultural and transboundary groups. 

For the past twelve years she has been involved in education a capacity building projects related to wetland management, water governance and sustainability science. She corrently works as an Academic Technician at the National Laboratory of Sustainability Sciences, Institute of Ecology (LANCIS-IE), UNAM in research projects related to the design and facilitation of participatory processes that promote transformations towards sustainability in Mexico. She is part of the international Pathways Network, where she co-coordinated the North American hub, one of six hubs that conform the Pathways to Sustainability Global Consortium, STEPS Center, United Kingdom.

To learn more about her projects and publications:
<http://lancis.ecologia.unam.mx/personal/lakshmi>
