---
title: Dr. Beth Tellman
role: Scientific Adviser
slug: team/beth_tellman
status: hidden
headshot: /theme/images/equipo/beth_tellman.png
template: perfil
spanish: https://umbela.org/equipo/beth_tellman/
---

From a human-environmental geography perspective, Beth seeks to address the causes and consequences of global environmental change in vulnerable populations, with a focus on access to water, flood risk, and land use change. She integrates qualitative and quantitative methods in social and natural sciences, including remote sensing, machine learning, natural language processing, geographical information systems, and statistics. Her field work focuses on Mexico and Central America, with publications on the politics of informal settlements in Mexico City, the history of water vulnerability and adaptation in Mexico City, narcotrafficking and forest loss in Central America, and flooding due to urbanization in El Salvador. She is a postdoctoral scientist at Columbia University's Earth Institute and the ACToday Project ("Adapting Agriculture to the Climate Today, for Tomorrow") working on index-based flood insurance in Bangladesh. She is a co-founder of Cloud to Street, a public benefit corporation that leverages remote sensing data to build flood monitoring and mapping systems for low- and middle-income countries. Her PhD is in Geography from Arizona State University. Currently she is a researcher at the <a href="https://geography.arizona.edu/people/beth-tellman">University of Arizona</a>.

To learn more about her publications and projects:
<https://beth-tellman.github.io/>
