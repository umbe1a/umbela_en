---
title: Dr. Paola Massyel García Meneses
role: Scientific Adviser
slug: team/paola_garcia
headshot: /theme/images/equipo/paola_garcia.png
status: hidden
template: perfil
spanish: https://umbela.org/equipo/paola_garcia/
---

A graduate biologist from UNAM's Faculty of Sciences, she holds a PhD in Science from the University of Plymouth, United Kingdom. Her work has focused mainly on the study of socio-ecological processes at different temporal and spatial scales, from the study of processes such as pollination to processes of global change mainly in Latin American systems. Within her professional career she has held positions in the public sector as Deputy Director of Biological Community Conservation and Adaptation to Climate Change, as well as in organizations such as the United Nations and academia. She has articles published in international journals, technical reports and book chapters. She is currently a researcher at the National Laboratory of Sustainability Sciences (LANCIS), UNAM's Institute of Ecology where she works on projects in the areas of vulnerability, adaptation and resilience to global changes, monitoring and evaluation of socio-ecological systems, as well as impact research and linkage of science promoting the involvement of other key sectors for decision-making. She has taught in the United Kingdom, Ecuador and Mexico. She is a representative within the Working Committees on Climate Change and Sustainable Cities of the UN Sustainable Development Solutions Network (SDSN) among others. 

To learn more about her publications and projects:  
<http://lancis.ecologia.unam.mx/personal/paola_garcia>
