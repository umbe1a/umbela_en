---
title: Dr. Patricia Pérez-Belmont
role: Executive Director
slug: team/patricia_perez
headshot: /theme/images/equipo/patricia_perez.png
status: hidden
template: perfil
spanish: https://umbela.org/equipo/patricia_perez/
---

Patricia works in the field of transformations for sustainability in socio-ecological contexts through the creation and facilitation of participatory and collaborative spaces and processes that seek to articulate diverse knowledge and cultures to foster 
collective action towards just and sustainable futures.

Patricia trained as a Biologist with a Master's degree in Biological Sciences and a PhD in Sustainability Sciences; all her studies have been carried out at the National Autonomous University of Mexico (UNAM). Patricia has broad experience in the design and coordination of projects with diverse social actors using inter and transdisciplinary approaches, participatory tools and methodological pluralism. Among the topics worked on in these projects are the creation of collaborative networks for sustainability, implementation of sustainable productive practices for conservation, monitoring of aquatic socio-ecological systems, citizen science, among others.

As part of her career and professional training, she has been involved in adult education and literacy, as well as teaching biology at high school and undergraduate levels at the Faculty of Sciences of the UNAM. Patricia has published scientific and dissemination publications, has participated in both national and international conferences and as an associate editor in peer-reviewed journals. Due to her experience and trajectory, she was invited by the US Department of State through the Bureau of Educational and Cultural Affairs of its embassy in Mexico to participate in the ‘International Visitor Leadership Program, Environmental Engagement and the Economy’. 

Patricia is currently the Director of Umbela Transformaciones Sostenibles and also collaborates with the University Seminar on Society, Environment and Institutions (SUSMAI) at UNAM.

<https://www.researchgate.net/profile/Patricia-Perez-Belmont>

<https://www.linkedin.com/in/patricia-p%C3%A9rez-belmont-a261397a/>
