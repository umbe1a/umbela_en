---
title: Dr. Shiara González Padrón
role: Co-founder
slug: team/shiara_gonzalez
headshot: /theme/images/equipo/shiara_gonzalez.png
status: hidden
template: perfil
spanish: https://umbela.org/equipo/shiara_gonzalez/
---

Graduated in Veterinary Medicine from the Universidad Centroccidental Lisandro Alvarado (UCLA), Venezuela, from where she originates. She first came to Mexico in 2010, where she did a professional internship at SEMARNAT's General Directorate of Wildlife, and returned in 2011 to start her Master of Science within the Department of Wildlife at UNAM's Faculty of Veterinary Medicine. 

She has a phD in Sustainability Sciences, from UNAM's Graduate program in Mexico City. Her research focused on the changes that have occurred since the introduction of rainwater collection systems in two indigenous communities (Wixáritari) in Jalisco, Mexico. For ten years she has collaborated with different civil society organizations (Isla Urbana, Proyecto ConcentrArte, Desarrollo Rural Sustentable Lu'um and IRRI México) to meet various needs in marginalized indigenous communities. 

In recent years she has served as field coordinator and supported research, evaluation and resource management of the eco-technology area in the Ha Ta Tukari project (water our life). Her main area of interest is the research of the processes of change and transformation that occur from multiple disciplines to promote socio-ecological systems towards environmental sustainability, human well-being and social justice in Mexico and countries of the Global South. 
