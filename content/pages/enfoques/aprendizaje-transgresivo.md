---
title: Transgressive learning
subtitle: Transgressive learning
slug: enfoques/Transgressive-learning
no_banner: True
status: hidden
spanish: https://umbela.org/enfoques/aprendizaje-transgresivo/
---

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-6">
<center>
<object type="image/svg+xml" data="/theme/images/enfoques_aprendizaje-transgresivo.svg" width="100%" height="220%"></object>
</center>
</div>

<div class="col-md-6">
<h2>Transgressive learning</h2>
<p>Transgressive learning is a form of action-oriented collective learning that seeks empowerment through deeper and more reflexive ways of knowing. This approach implies proposing more radical ways of questioning and representing what transformations to sustainability could mean in various contexts, and challenges what has been normalized (i.e., business as usual, status quo). Transgressive learning focuses on generating critical thinking through mechanisms that intentionally promote changes in dominant and unjust practices, and on enabling collective agency. These approaches are designed to transgress and explore how other worlds are possible from affectivity, care, and ethical responsibility.</p>

<p><i>To read more about this approach, we recommend the following texts:</i></p>
<ul>
<li><a href="https://transgressivelearning.org/">T-Learning</a></li>
<li>Thomas Macint Yre, Martha Chaves, Dylan McGarry. 2018. <a href="https://drive.google.com/file/d/1wXDvsiHDO9GLJ19zx4WjdnbNt8VvcvU7/view?usp=sharing">Living Spiral Network</a></li>
<li>Thomas Macint Yre, Martha Chaves, Dylan McGarry. 2018. <a href="https://drive.google.com/file/d/1wXDvsiHDO9GLJ19zx4WjdnbNt8VvcvU7/view?usp=sharing">Marco conceptual del espiral vivo.</a></li>
<li>Lotz-Sisitka et al. 2015. <a href="https://drive.google.com/file/d/1LQ88zWieZw2AE2MnFR781h5JVCkcstkP/view?usp=sharing">Transformative, transgressive social learning: rethinking higher education pedagogy in times of systemic global dysfunction.</a></li>
</ul>

</div>

</div>
