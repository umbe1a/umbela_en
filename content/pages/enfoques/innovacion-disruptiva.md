---
title: Disruptive Innovation
subtitle: Disruptive Innovation
slug: enfoques/Disruptive-innovation
no_banner: True
status: hidden
spanish: https://umbela.org/enfoques/innovacion-disruptiva/
---

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-6">
<center>
<object type="image/svg+xml" data="/theme/images/enfoques_innovacion-disruptiva.svg" width="100%" height="220%"></object>
</center>
</div>

<div class="col-md-6">
<h2>Inclusive innovation</h2>
<p>Inclusive innovation comprises initiatives such as products, processes, programs, projects or platforms that seek to challenge perceptions and beliefs about routines, resources and power relations that define the distribution of a good or service in an unequal, unfair and inequitable (transgressive) way. Inclusive innovations can be simple, accessible and low-cost solutions aimed at increasing the access of unprivileged, marginalized and traditionally excluded populations to opportunities and socioeconomic capital (frugal). In addition, inclusive innovations can occur in diverse community and civil society arenas, involving change agents experimenting with social innovations to meet community socio-environmental needs, including the diversity of alternative knowledge and knowledges to conventional (grassroots or community) ways of understanding the world. In this case, such innovations can be created collectively (in communities of practice, indigenous, or local movements) and with existing resources and materials. An example would be rainwater harvesting, so as not to have to pay or depend on the State or policies to receive water at home, and thus create options that the user communities themselves can implement. In this way, social innovations can contribute to a social economy, as part of community activities and social entrepreneurship. Finally, inclusive innovations can contribute to trigger transformative changes in a system that affect multiple scales and affect more than one group, organizations and specific communities, thereby aiming to impact the entire system (e.g. cultural, legal, economic changes, etc.).</p>

</div>

</div>
