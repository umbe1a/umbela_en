---
title: Decolonising
subtitle: Decolonising
slug: enfoques/Decolonising
no_banner: True
status: hidden
spanish: https://umbela.org/enfoques/descolonizacion/
---

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-6">
<center>
<object type="image/svg+xml" data="/theme/images/enfoques_descolonizacion.svg" width="100%" height="200%"></object>
</center>
</div>

<div class="col-md-6">
<h2>Decoloniality</h2>
<p>The decoloniality approach implies making explicitly visible the structures and ways of thinking that have persisted even after the independence of nations (decolonization). Decoloniality invites us to accept the diversity of knowledges, ways of thinking and knowing the world, to recognize different cultures and to fight against oppression, systems of domination and discrimination that have appeared under different historical configurations in different territories. This approach places the political dimension associated with situations of injustice and marginalization at the center of the analysis. For this, it is crucial to promote processes of collective reflexivity aimed at challenging the root causes of sustainability problems, in order to promote more horizontal and inclusive movements and structures that enable action towards a common good.</p>

<p><i>To read more about this approach, we recommend the following texts:</i></p>
<ul>
<li>Eduardo Restrepo, Axel Rojas. 2010. <a href="https://drive.google.com/file/d/1i_XExvCz4XWmpcCtBgGzIYziaAvhs3Ix/view?usp=sharing">Inflexión decolonial: fuentes, conceptos y cuestionamientos.</a> 
</li>
<li>Adela Parra-Romero. 2016. <a href=https://www.ecologiapolitica.info/?p=6006>¿Por qué pensar un giro decolonial en el análisis de los conflictos socioambientales en América Latina?</a>
</li>
</ul>

</div>

</div>
