---
title: Transdisciplinarity
subtitle: Transdisciplinarity
slug: enfoques/Transdisciplinarity
no_banner: True
status: hidden
spanish: https://umbela.org/enfoques/transdisciplina/
---

<div class="row mb-12 bg-light" style="padding:1em">

<div class="col-md-6">
<center>
<object type="image/svg+xml" data="/theme/images/enfoques_transdisciplina.svg" width="100%" height="220%"></object>
</center>
</div>

<div class="col-md-6">
<h2>Transdisciplinarity</h2>
<p>Transdisciplinarity is understood as a relational and integrative process of different types of knowledge and perspectives where the importance of non-academic and non-disciplinary knowledges is emphasized. It operates through processes of dialogue, collaboration and constant interaction between theory-practice, values, and interests. This collaborative effort focuses on addressing social-environmental problems and finds in transdisciplinarity a reflexive framework that makes it possible to integrate knowledges from different sectors and bodies of knowledge. Transdisciplinarity enables joint learning, new relationships of cooperation and self-organization and seeks to bring knowledges into action to foster transformations towards the sustainability of social-environmental systems that promote deep and lasting changes with a vision of justice.</p>

<p><i>To read more about this approach, we recommend the following texts:</i></p>
<ul>
<ul>
<li>Juliana Merçon, Bárbara Ayala-Orozco y Julieta A. Rosell. 2018. <a href="https://drive.google.com/file/d/1LTDusAxv2bJHwrirBE3HmKFmKzeG405i/view?usp=sharing">Experiencias de colaboración transdisciplinaria para la sustentabilidad.</a></li>
<li>Russell A.W., et al. 2008. <a href="https://drive.google.com/file/d/1LSLaI9OEIxxKVJE2kY_EAxhLZs_GXaaY/view?usp=sharing">Transdisciplinarity: Context, contradictions and capacity.</a></li>
<li>Lang D.J., et al. 2012. <a href="https://drive.google.com/file/d/1LSLJQOxc5sVLpaHUULqdnvl8_BWZuiSk/view?usp=sharing">Transdisciplinary research in sustainability science: practice, principles, and challenges.</a></li>
</ul>

</div>

</div>
