---
title: Start
url: /
save_as: index.html
template: raiz
title_image: /theme/images/bg_inicio.jpg
fa_icon: fa-arrow-circle-o-right
subtitle: Weaving transformations towards sustainability
status: hidden
spanish: https://umbela.org/
---


<br />

To design pathways towards the sustainability of the Earth and try to address its socio-environmental problems, particularly those in the Global South, there is no single solution, not one approach, nor can be solved with a single discipline, way of knowing, or person- ... but there is plurality that has sometimes led to contradictions and asymmetries. That is why we believe it is necessary to think differently, from the inside out and vice versa, stopping to understand that there are different realities and ways of seeing the same Earth. In those other spaces where there are individuals who may be different from us and far from where we stand, and perhaps, after introspection, we can look for ways to create an "us", a team, a community, a network, to jointly weave paths towards a more sustainable Earth.

----

