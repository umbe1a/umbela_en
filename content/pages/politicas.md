---
title: Terms of use and privacy policy
slug: politicas
no_banner: True
status: hidden
spanish: https://umbela.org/politica/
---

###Umbela Sustainable Transformations, A.C.

###Terms of use

This website umbela.org is owned and operated by Umbela Transformaciones Sostenibles A.C. These "Terms and Conditions of Use" establish the terms and conditions under which you may use our website and services offered by us. This website offers visitors the description of our mission and vision, our work approaches, the network that conforms "Umbela Transformaciones Sostenibles, A. C.", (founders, partners, advisors, consultants, consultants and consultants), as well as the projects we carry out and the way in which you can link with us using the contact tab. By accessing or using the web page of our service, you approve that you have read, understood and agreed to be bound by these Terms:

- Ownership of intellectual property, copyrights and logos

The Service and all materials included or transferred, including without limitation, software, images, text, graphics, logos, patents, trademarks, service marks, copyrights, photographs, audio, videos, music, and all Intellectual Property Rights related thereto, are the exclusive property of "Umbela Transformaciones Sostenibles, A. C." Except as explicitly stated herein or in the section of the corresponding page, nothing in these Terms shall be deemed to create a license under any such Intellectual Property Rights. You agree not to sell, license, rent, modify, distribute, copy, reproduce, transmit, publicly display, publish, adapt, edit or create derivative works therefrom.

- Limitation of liability

To the maximum extent permitted by applicable law, in no event shall "Umbela Transformaciones Sostenibles, A. C." be liable for indirect, punitive, incidental, special, consequential or exemplary damages, including, but not limited to, damages for loss of profits, goodwill, use, data or other intangible losses, arising out of or related to the use or inability to use our services and content. 

To the maximum extent permitted by applicable law, "Umbela Transformaciones Sostenibles, A. C." assumes no liability for (i) content errors or inaccuracies; (ii) personal injury or property damage, of any nature whatsoever, resulting from your access or use of our service and content; and (iii) any unauthorized access or use of our secure servers and/or all personal information stored therein.

- Right to change and modify the Terms

We reserve the right to modify these Terms from time to time at our sole discretion. When we change the Terms in a material way, we will notify you that material changes have been made to the Terms. Your use of the website or our service after such change constitutes your acceptance of the new Terms. If you do not agree to any of these Terms or any future version of the Terms, do not use or continue to access the Website or our services.

- Promotional e-mails and content

You agree to receive our messages and promotional materials by email or any other contact form in which you provide us with your personal information (including your phone number for calls or text messages). If you do not wish to receive such materials or promotional notices you may notify us at contacto@umbela.org.

###Privacy policy

In compliance with the Federal Law for the Protection of Personal Data in Possession of Individuals (the "Law"), and other related laws, we hereby inform you about the characteristics and purposes of the treatment that will be given to your personal data that Umbela Transformaciones Sostenibles A. C. (Umbela) carries out in the course of its activities. (Umbela).

- Identity and address of the person in charge

Umbela is a Civil Association that aims to promote the transformation of the world towards sustainability, through projects that generate collaborative spaces to articulate the diversity of knowledge, cultures, approaches and knowledge that contribute to develop and increase conditions of welfare, justice and equity in society and the environment. 

Umbela is located in Mexico City, Mexico and is responsible for collecting your personal data, the use and storage of such data and its due protection.

- Data collected

Regarding the information of your personal data, this may be provided personally and directly by you, either verbally or in writing, by e-mail or correspondence, business cards, or by filling out contact forms and personal data collected through our website, or any other means.

We collect the following personal data from you:
First and last name
E-mail address

- What do we use your personal data for?

To verify identity and respond to your requests between you and Umbela.
To agree our services.
To follow up on services purchased or contracts entered into.
To send you information about our events, calls, and other initiatives related to Umbela.
To analyze information for statistical purposes on the impact of our projects.

In case you do not want your personal data to be used for these purposes, please let us know by sending an email to contacto@umbela.org.
The refusal to use your personal data for these purposes may not be a reason for us to deny you the services and products you request or contract with us.

- Where can I view the privacy notice?

For more information about the terms and conditions under which your personal data will be treated, such as the third parties with whom we share your personal information and how you can exercise your ARCO rights, you can consult the comprehensive privacy notice at: umbela.org or you can request it by sending an email to contacto@umbela.org.
Additional Information

I. It is of our special interest to take care of sensitive personal information, that which reveals aspects such as racial, ethnic origin, health status, genetic information, gender information, religious, philosophical and moral beliefs, union membership, political opinions, sexual preferences, about minors, people with disabilities and people who have been declared in interdiction. This data will be processed only for the purposes described in this Privacy Notice.

II. The personal data collected will be part of a database that will remain in force for as long as Umbela exists, or for the period necessary to fulfill the aforementioned purpose, so we will adopt each and every one of the administrative, physical and technical security measures established by law, necessary to safeguard your personal information from any damage, loss, alteration, destruction or unauthorized use, access, or treatment.

III. If you consider that your right to the protection of your personal data has been violated by any conduct or omission on our part, or presume any violation of the provisions of the Federal Law for the Protection of Personal Data in Possession of Individuals, its Regulations and other applicable laws, you may file a complaint with the National Institute for Transparency, Access to Information and Protection of Personal Data (INAI). For more information, we suggest you visit its official website www.inai.org.mx.

IV. By providing us with your data, you give your consent for your personal data to be treated in accordance with the provisions of this privacy notice or if you wish, you can do so expressly by sending an email to: contacto@umbela.org, with the subject: "Consent"; adding in the message the following legend: "I give my consent for my personal data to be treated in accordance with the provisions of this privacy notice".

- Modifications to the privacy notice

This privacy notice may undergo modifications, changes or updates derived from new legal requirements; from our own needs for the products or services we offer; from our privacy practices; from changes in our work model, or from other causes.

We commit to keep you informed about the changes that this privacy notice may undergo through our web page: umbela.org.

Date of update: April 2021.

