---
title: About us
subtitle: Question, explore and create new ways of understanding the world
slug: about-us
title_image: /theme/images/bg_acerca.jpg
fa_icon: fa-bookmark-o
spanish: https://umbela.org/acerca/
---



Umbela is an organization that aims to foster transformations towards sustainability, through innovative processes that generate spaces of collaboration to articulate the diversity of knowledges, cultures, approaches and wisdom that contribute to develop and increase conditions of well-being, justice, and equity in social-environmetal systems.


<div style="width: 28rem; margin: 0 0 1em 3em;" class="float-md-right">
<h2>Vision</h2>
<p style="font-weight: 100;">
Leverage sustainability science to foster transformation 
processes towards just, inclusive and equitable trajectories 
in society and the environment.
</p>

<h2>Mission</h2>
<p style="font-weight: 100;">
Promote transformations towards sustainability in socio-environmental 
systems through strengthening capacities, articulating the diversity 
of knowledges, facilitating participatory spaces and processes of 
collaboration, and implementing innovative solutions.
</p>
</div>


Our primary objective is to question, explore, and create new ways of understanding the world to seek solutions to social-environmental problems that currently jeopardize the sustainability of our planet. We beleive that there is collective will to make change possible, and that is why we want to contribute to supporting such change by seeking new meanings and understandings, deconstructing and rebuilding through our projects with the following particular objectives:

- Strengthen capacities of different actors in society through tools of adaptive collaboration, inclusion, and plurality.
- Give voice to marginalized groups and voices in situations of socio-environmental conflict.
- Seek innovative and collaborative solutions to contribute to social well-being and global environmental integrity.
- Promote transdisciplinary processes that transcend disciplines, institutions, and hierarchies to co-construct visions and decisions that catalyze paths of transformation towards sustainability.
- Build alliances, and spaces of dialogue and collaboration through iterative participation 

----
 
### Umbela's work is inspired and guided by four core values:

(Click on each value to see its description)

<center>
<object type="image/svg+xml" data="/theme/images/valores.svg" width="40%" height="100%"></object>
</center>

----

