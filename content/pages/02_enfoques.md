---
title: Approaches
subtitle: De-construct to re-construct
slug: Approaches
title_image: /theme/images/bg_enfoques.jpg
fa_icon: fa-hourglass-o
spanish: https://umbela.org/enfoques/
---

At Umbela we consider that the hierarchical structure of today's
hegemonic system does not comprehensively address the critical 
problems of the world and does not recognize biological and social 
diversity (classes, gender, cultures, ethnicities), or the value of 
resistance and regeneration movements seeking to protect such diversity. 
In addition, we recognize that the processes of colonization, extractivism, 
racism, sexism and other forms of discrimination and oppression have 
generated very deep historical inequity. The consequences of this inequity 
marginalize and disadvantage voices of groups that have been denied the same 
power as dominant groups (e.g., those associated with visions of the West, 
Anglo-Saxons, the Global North, and/or sexists). Throughout history, many 
narratives and ideas have been extracted from their local contexts to 
depoliticize and make them marketable, while marginalized voices have been 
silenced in the construction of widsom and knowledge, both in scientific 
fields and in governance systems. 

Therefore, at Umbela we recognize the need to shape deeper processes of 
change towards fairer and more sustainable societies. These types of changes 
are called transformations, and seek to generate root changes in paradigms, 
values, behaviors, practices, or norms. Transformations towards sustainability 
require transgressing the dominant structures and narratives that maintain 
poverty, inequality, and socio-ecological degradation, and thus it is necessary 
to explicitly make visible and challenge what is normalized (such as colonial 
practices, extractivism, over-consumption, or socio-environmental injustice). 
For Umbela it is essential to create spaces of dialogue between the South and 
the Global North, spaces in which it is explicitly recognized that the production 
and exchange of knowledge and practices are necessary from all possible corners 
and trenches, always respecting and recognizing their origins, their particular 
contexts, their authors, and their struggle. 

----

<div class="row mb-12 white" style="padding:1em">

<div class="col-md-6">
<h2>Theoretical-methodological approaches</h2>
Based on our notions and values, Umbela follows four main approaches in our work:

<ul>
<li>Inclusive innovation</li>
<li>Transgressive learning</li>
<li>Transdisciplinarity</li>
<li>Decoloniality</li>
</ul>

Click on each approach on the image to see its description and references -->
</div>

<div class="col-md-6">
<center>
<object type="image/svg+xml" data="/theme/images/enfoques.svg" width="70%" height="130%"></object>
</center>
</div>

</div>

<div class="row mb-12 white" style="padding:1em">

<div class="col-md-6">
<h2>The work of Umbela</h2>
Umbela develops and collaborates in projects that contribute to exploring trajectories towards sustainable transformations, using one or more of our theoretical-methodological approaches. Throughout our professional work, we have collaborated with areas of the public and private sectors, civil society organizations and academia, prioritizing participatory action research schemes on topics such as: collective agency; collaborative governance; capacity building; rural, peri-urban and urban development and territorial planning; and strategies related to climate change.
</div>

<div class="col-md-6">
<h2>Capabilities</h2>
<img alt="Capabilities" src="/media/capacities.png" width="100%" />
</div>

</div>


### Type of products

- Tools and guides for transdisciplinary work
- Design of spaces for participatory processes
- Boundary objects for communication between diverse communities of practice (maps, games, etc.)
- Monitoring, reporting and verification systems (MRV)
- Computational platforms and models
- Capacity building analysis in different sectors
- Educational and outreach materials

<center>
<img src="/media/areas_1.jpg" width="23%" />
<img src="/media/areas_2.jpg" width="23%" />
<img src="/media/areas_3.jpg" width="23%" />
<img src="/media/areas_4.jpg" width="23%" />
</center>

----

### How can we work with you or your organization?

If you have any idea or proposal in which you think we can collaborate together, please write to [our contact](https://en.umbela.org/contact/) to discuss possible ways of interacting. Umbela is a non-profit civil organization, so we prioritize horizontal collaboration schemes in the form of consortiums and/or strategic alliances with other civil society organizations, academic institutions, private sector, etc.
