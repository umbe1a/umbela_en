---
title: Equity
subtitle: Equity
slug: valores/Equity
no_banner: True
status: hidden
spanish: https://umbela.org/valores/equidad/
---

<center>
<object type="image/svg+xml" data="/theme/images/valores_equidad.svg" width="40%" height="100%"></object>
</center>

<br />

We encourage schemes that address asymmetry and make visible marginalizing 
narratives and dynamics to promote current and future social welfare conditions.

<br />
