---
title: Plurality
subtitle: Plurality
slug: valores/Plurality
no_banner: True
status: hidden
spanish: https://umbela.org/valores/pluralidad/
---

<center>
<object type="image/svg+xml" data="/theme/images/valores_pluralidad.svg" width="40%" height="100%"></object>
</center>

<br />

We recognize and celebrate the diversity of contexts and ways of 
knowing and feeling the world, the rights of all forms of life 
(of present and future generations), based on concepts of social 
claims of injustice and ecological equilibrium. 

<br />
