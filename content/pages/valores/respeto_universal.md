---
title: Universal respect
subtitle: Universal respect
slug: valores/Universal-respect
no_banner: True
status: hidden
spanish: https://umbela.org/valores/respeto-universal/
---

<center>
<object type="image/svg+xml" data="/theme/images/valores_respeto_universal.svg" width="40%" height="100%"></object>
</center>

<br />

We see the Earth as a single connected system in which all living 
beings and their interrelationships have the same level of importance, 
therefore deserving the same respect and empathy.

<br />
