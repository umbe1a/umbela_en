---
title: Environmental justice
subtitle: Environmental justice
title_image: /theme/images/bg_justicia_ambiental.jpg
slug: valores/Environmental-justice
no_banner: True
status: hidden
spanish: https://umbela.org/valores/justicia-ambiental/
---

<center>
<object type="image/svg+xml" data="/theme/images/valores_justicia_ambiental.svg" width="40%" height="100%"></object>
</center>

<br />

We consider that the nature-human duality in the anthropocene era 
has generated dispossession of resources, territories and knowledges, 
so we seek to promote participatory spaces and self-governance that 
generate conditions of greater justice.

<br />
