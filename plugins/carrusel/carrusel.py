from pelican import signals

def article_slides(sender):
    slides = []

    for a in sender.articles:
        if hasattr(a, 'carrusel'):
            slides.append(a)

    sender.context['carrusel'] = slides




def page_slides(sender):
    slides = []
    
    for p in sender.pages:
        if hasattr(p,'carrusel'):
            slides.append(p)

    sender.context['carrusel'] += slides


    
def register():
    signals.article_generator_finalized.connect(article_slides)
    signals.page_generator_finalized.connect(page_slides)


